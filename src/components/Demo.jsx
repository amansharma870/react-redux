import React from 'react';
import {useSelector,useDispatch} from 'react-redux'
import {Increse_Item,Decrese_Item} from '../Redux/Action/index.js'
function Demo() {
   const myState=useSelector((state)=>state.IncDec)
   const dispatch=useDispatch()
    return (
        <div>
            <button onClick={()=>dispatch(Decrese_Item())}>-</button>
            <input type="text" value={myState}/>
            <button onClick={()=>dispatch(Increse_Item())}>+</button>
        </div>
    );
}

export default Demo;